﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player3 : MonoBehaviour {

	public int coinCount = 0;
	public GameObject winbox;
	public Text coinBG;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}



	void OnCollisionEnter (Collision col)
	{
		if(col.gameObject.tag == "Collect")
		{
			Debug.Log("COIN!!");
			coinCount++;
			Destroy(col.gameObject);
			coinBG.text = coinCount.ToString();

			if(coinCount == 4){

				win();
			}

		}
	}


	void win () {

		Debug.Log("win");
		winbox.SetActive(true);
	}


}
